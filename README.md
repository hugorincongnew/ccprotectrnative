radius
===================


## Setup
   1. Intall the gems with bundler

      ```
      $ gem install bundler
      $ bundle install
      ```

   2. Install the pods
      Update the CocoaPods master repo

      ```
      $ [bundle exec] pod setup
      ```

      To tell motion-cocoapods to download your dependencies, run the following rake task:

      ```
      $ [bundle exec] rake pod:install
      ```

## Running the app
   On the simulator:

   ```
   $ [bundle exec] rake simulator
   ```
   
   On the a device:

   ```
   $ [bundle exec] rake device
   ```   
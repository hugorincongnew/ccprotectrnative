class AppDelegate < PM::Delegate
  include CDQ
  #include PM::DelegateNotifications

  #status_bar true, animation: :fade

  def on_load(app, options)
    cdq.setup
    
    #Fabric.with([Crashlytics])
    EMGOneTouchManager.sharedInstance.initializeWithOrganizationToken("e4ea2070e65bd079cb4367fd7f14fd8d20530ccf81646ba0c8617892ee3f27d5", apiKey:"7a7d23e7597428d081bafee70b650a60cb5785e3d966e0a7008cdbe2813f27a4", productionBuild:false)
       
    #Parse.setApplicationId("PPYdMeq7zXk1REn0dKISehPWaGxdYIrILzUmVMfx", clientKey:"kFOsxi909JiXvV2sEN2D2YTQOOSfrcSQtqBnPmYp")
    #register_for_push_notifications :all
    UIApplication.sharedApplication.applicationIconBadgeNumber = 0;

    App::Persistence["#{App::Persistence['user_id']}-no_subscription"] = true if App::Persistence["#{App::Persistence['user_id']}-no_subscription"] == nil
    

    if Device.ios_version.to_f >= 8
      BW::Location.location_manager.requestWhenInUseAuthorization
    end

    App::Persistence['checked'] = false    
    
    if App::Persistence['logged_in'] == true #&& App::Persistence['authToken'] != nil
      if App::Persistence['non_cc_showing'] == "true"
        open ShowingScreen.new(nav_bar: false)
      else
        open_root_screen HomeScreen.new(nav_bar: false)
      end    
    else
      open WelcomeScreen.new(nav_bar: false)
    end
  end

  # Remove this if you are only supporting portrait
  def application(application, willChangeStatusBarOrientation: new_orientation, duration: duration)
    # Manually set RMQ's orientation before the device is actually oriented
    # So that we can do stuff like style views before the rotation begins
    device.orientation = new_orientation
  end

  def on_push_registration(token, error)
    if token
      #PFPush.storeDeviceToken(token)
      #PM.logger.info token.description
      #clean_token = token.description.gsub(" ", "").gsub("<", "").gsub(">", "")
      #App::Persistence['device_token'] = clean_token
    else
      App.alert(error)
    end
  end

  def on_push_notification(notification, launched)
    #notification.to_json  # => '{"aps":{"alert":"My test notification","badge":3,"sound":"default"}, "custom": "Jamon Holmgren"}'
    #notification.alert    # => "My test notification"
    #notification.badge    # => 3
    #notification.sound    # => "default"
    #notification.custom   # => "Jamon Holmgren"
  end

  #- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
  def application(application, didReceiveRemoteNotification:userInfo)
    #show_alert("Push Notification Received", "Alert: #{userInfo['aps']['alert']}\n\nSound:#{userInfo['aps']['sound']}\n\nBadge: #{userInfo['aps']['badge']}\n\nData: #{userInfo['aps']['custom_data']}")

    #if userInfo['aps']['alert'] == "Are you OK"
      # receive_showing_danger_confirm
      #end
  end

  #- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
  def application(application, didFailToRegisterForRemoteNotificationsWithError:error)
    # Failed to register for push notifications
  end

  def begin_showing
    p "starting the showing"

    App::Persistence['non_cc_showing'] = "true"
    #create showing though api
    headers = { 'Authorization' => 'Token token="13ee4d1a3b6fe8cdaee7712f8cfe93caf5ef10b9"' }
    @data = {user_email: App::Persistence['user_email'], user_token: App::Persistence['authToken'], agent_id: "#{App::Persistence['user_id']}", agent_name: "#{App::Persistence['user_name']}" }
    BW::HTTP.post("#{API_URL}/api/v1/users/#{App::Persistence['user_id']}/visits", { headers: headers, payload: @data}) do |response|
      p response
      if response.status_code == 201
        p json = BW::JSON.parse(response.body.to_str)
        App::Persistence['showing_id'] = json['id']
        open ShowingScreen.new
      else
        App.alert("oops, something went wrong. Please try that again...")
        open_root_screen HomeScreen.new(nav_bar: false)
      end
    end
  end

  def get_contacts

    $contacts = []
    @data = {}
    headers = { 'Authorization' => 'Token token="13ee4d1a3b6fe8cdaee7712f8cfe93caf5ef10b9"' }
    BW::HTTP.get("#{API_URL}/api/v1/users/#{App::Persistence['user_id']}/emergency_contacts", { payload: @data, headers: headers }) do |response|
      p response
      if response.ok?
        p json = BW::JSON.parse(response.body.to_str)
        if json.count >= 1
          p json.first['id']
          json.each do |contact|
            unless contact['name'] == nil
              $contacts << {api_id: contact['id'], name: contact['name'], phone: contact['phone'], email: contact['email'], last_name: contact['really_last_name'].to_s}
            end
          end
        end
        App::Persistence['contacts'] = @contacts
        open EmergencyContactsScreen.new(nav_bar:true)
      else
        App::Persistence.delete('auth_token')
      end
    end
  end
end

# production
#HOST = "https://sharknado-safety.herokuapp.com"

# staging
HOST = "http://tranquil-scrubland-38364.herokuapp.com"


class API

  def base
    HOST
  end

  def registration
    base + "/api/v1/users.json"
  end

  def sessions
    base + "/api/v1/users/sign_in.json"
  end

  def logout
    base + "/api/v1/users/sign_out.json"
  end

  def braintree_token
    base + "/api/v1/client_token"
  end

  def payment
    base + "/api/v1/subscriptions"
  end

  def discounts
    base + "/api/v1/discounts.json"
  end

  def update_credit_card
    base + "/api/v1/update_credit_card.json"
  end

  def create_credit_card
    base + "/api/v1/credit_cards.json"
  end

  def forgot_password
    base + "/api/v1/users/password/new"
  end

  def bill_plans
    base + "/api/v1/plans.json"
  end

  def new_contact
    base + "/api/v1/emergency_contacts.json"
  end

  def emergency_contacts
    base + "/api/v1/users/"+App::Persistence['user_id'].to_s+"/emergency_contacts"
  end

  def emergency_contact(contact_id)
    base + "/api/v1/users/"+App::Persistence['user_id'].to_s+"/emergency_contacts/#{contact_id}.json"
  end

  def user_payment
    base + "/api/v1/users/"+App::Persistence['user_id'].to_s+"/subscriptions.json"
  end

  def iap_subscription
    base + "/api/v1/iap_subscriptions"
  end

  def subscriptions(id)
    base + "/api/v1/users/"+App::Persistence['user_id'].to_s+"/subscriptions/#{id}.json"
  end  
  
  def user
    base + "/api/v1/users/"+App::Persistence['user_id'].to_s
  end 

end

# prod
API_URL = API.new.base
API_PRIVACY_POLICY_URL = "http://curbcall.com/privacy/"
API_TERMS_URL = "http://curbcall.com/terms/"

# labs
#API_URL = "https://curbcalllabs.herokuapp.com"
#
SESSIONS_URL =  API.new.sessions

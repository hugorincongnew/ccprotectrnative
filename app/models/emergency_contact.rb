class EmergencyContact < CDQManagedObject
  def initialize(params = {})
  end

  def update(params = {})
    self
  end

  def delete
  end

  def save
    true
  end

  class << self
    def create(params = {})
      EmergencyContact.new(params).tap do |emergency_contact|
        emergency_contact.save
      end
    end

    def find(id_or_params)
    end

    def all
      EmergencyContact.find(:all)
    end
  end
end

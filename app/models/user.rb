class User

  def self.id
    App::Persistence['user_id']
  end

  def self.email
    App::Persistence['user_email']
  end

  def self.first_name
    App::Persistence["#{App::Persistence['user_id']}-firstname"]
  end

  def self.last_name
  	App::Persistence["#{App::Persistence['user_id']}-lastname"]
  end
  
  def self.name
    App::Persistence["#{App::Persistence['user_id']}-name"]
  end  
  
  def self.phone
    App::Persistence["#{App::Persistence['user_id']}-phone"]
  end  
  
  def self.subscribed?
    App::Persistence["#{App::Persistence['user_id']}-subscribed"] or self.valid_promo?
  end  
  
  def self.promo
    App::Persistence["#{App::Persistence['user_id']}-promo"].to_s.upcase
  end  
  
  def self.promo_date
    App::Persistence["#{App::Persistence['user_id']}-promo-date"]
  end    
  
  def self.promo_days_left
    mp App::Persistence["#{App::Persistence['user_id']}-promo-date"]
    30 - ((Time.at(NSDate.dateWithString(self.promo_date)) - Time.now) / 86400).to_i
  end    
  
  def self.need_info?
    return true if self.first_name.nil? or self.first_name == ""
    return true if self.last_name.nil? or self.last_name == ""
    return true if self.phone.nil? or self.phone == ""
    return false
  end  
  
  def self.unmut
    ["name", "phone", "lastname", "firstname", "promo", "subscribed", "promo-date", "no_subscription"]
  end  
  
  private
  
  def self.valid_promo?
    if self.promo == "TRIAL"
      if self.promo_days_left > 0
        return true
      end    
    end
    false  
  end  
end

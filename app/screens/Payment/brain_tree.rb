class SC
  class BrainTree
    attr_accessor :client_token, :client

    def load_braintree_client
      BW::HTTP.get(API.new.braintree_token) do |response|
        json = BW::JSON.parse(response.body.to_str)
        @client_token = json["token"]
        @client = BTAPIClient.alloc.initWithAuthorization(@client_token)
      end
      return self
    end

  end
end

module ContactFormValidator
  def valid_contact_info?(data)
    mp data.inspect
    return false unless valid_name?(data['name'])
    return false unless valid_phone?(data['phone'].to_s)
    return false unless valid_email?(data['email'])   
    true
  end 
 
  def valid_name?(name)
    if !name.nil?
      unless name.size > 0 and name != "" and name != nil and name.size < 30
        App.alert("Please include a valid Name for this contact.") 
        return false 
      end
    else
      App.alert("Please include a Name for this contact.") 
      return false
    end    
    true 
  end 
 
  def valid_phone?(phone)
    if !phone.nil?
      unless phone.size > 0 and phone != "" and phone != nil and phone.size < 30
        App.alert("Please include a valid Phone Number.") 
        return false
      end   
    else
      App.alert("Please include a Phone Number.") 
      return false
    end    
    true
  end
 
  def valid_email?(email)
    if !email.nil?
      if email.size > 0 and email != "" and email != nil
        unless valid_email_format?(email)
          App.alert("Please check the Email format.")   
          return false    
        end 
      else
        App.alert("Please include an Email.")   
        return false 
      end 
    else
      App.alert("Please include an Email.")   
      return false
    end    
    true
  end 
 
  def valid_email_format?(email)
   (email =~ /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i)
  end 
end  
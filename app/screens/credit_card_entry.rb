class CreditCardEntry < PM::XLFormScreen
  form_options on_save:   :submit_form
  nav_bar true , {
    toolbar: true,
    hide_nav_bar: false
  }
  def on_load
    @scbt = SC::BrainTree.new.load_braintree_client
    # display_alert_for_discount_alert
    display_explanation_alert
  end

  def form_data
    [{
      title: "",
      footer: "",
      cells: [{
      name: "card_number",
      title: "Card Number",
      type: :password,
      # value: "4111111111111111"
    },
      {
      name: "exp_month",
      title: "Expiration Month",
      value: "",
      type: :selector_push,
      options: {
      "01" => "01",
      "02" => "02",
      "03" => "03",
      "04" => "04",
      "05" => "05",
      "06" => "06",
      "07" => "07",
      "08" => "08",
      "09" => "09",
      "10" => "10",
      "11" => "11",
      "12" => "12"
    },
    }, {
      name: "exp_year",
      title: "Expiration Year",
      value: "",
      type: :selector_push,
      options: {
        "2016" =>  "16",
        "2017" =>  "17",
        "2018" =>  "18",
        "2019" =>  "19",
        "2020" =>  "20",
        "2021" =>  "21",
        "2022" =>  "22",
        "2023" =>  "23",
        "2024" =>  "24",
        "2025" =>  "25",
        "2026" =>  "26",
        "2027" =>  "27"
    },
    }, {
      name: "cvc",
      title: "CVC Code",
      type: :password,
      value: ""
    },{
      name: :submit,
      title: "Submit",
      type: :button,
      appearance: {
      font: UIFont.fontWithName('Helvetica Neue', size: 15.0),
      detail_font: UIFont.fontWithName('Helvetica Neue', size: 12.0),
      color: UIColor.whiteColor,
      etail_color: UIColor.whiteColor,
      background_color: UIColor.blueColor
    },
      on_click: -> (cell) {
      on_save(nil)
    }

    }]
    }]
  end

  def submit_form(data)
    puts "Saving it"
    @credit_card = CreditCard.new(data['card_number'], data['exp_month'], data['exp_year'], data['cvc'], App::Persistence['promo_code'])
    puts @credit_card.inspect
    if valid_cc_params?(data)
      tokenize_card(@credit_card)
    else
      display_error_for_missing_cc_details
    end
  end

  def valid_cc_params?(data)
    (data['card_number'] != "" &&  data['exp_month'] != "" && data['exp_year'] != "" && data['cvc'] != "" )
  end

  def display_explanation_alert
    # TODO
    rmq.app.alert(title: "Pricing Information", message: "CurbCall Protect is part of the CurbCall ecosystem of technology for real estate agents. If you already have a login for any CurbCall product, you can use that in the login screen. If your brokerage gave you a special code to use here, you will be prompted to add it next. Your fee may include your brokerage association fees. Otherwise, please enter your credit card information to gain access to our suite of mobile and web-based tools including Protect, the BrokerageDashboard, CurbCall Recruiting Matrix. Sound good?", actions: :yes_no) do |action_type|
      #SVProgressHUD.showWithStatus("Checking..", maskType:SVProgressHUDMaskTypeGradient)
      case action_type
        when :yes
          display_alert_for_discount_alert
        when :no
          open WelcomeScreen.new
      end
    end
    # App.alert("Pricing Information", message:"CurbCall Protect is part of the CurbCall ecosystem of technology for real estate agents. If you already have a login for any CurbCall product, you can use that in the login screen. If your brokerage gave you a special code to use here, you will be prompted to add it next. Your fee may include your brokerage association fees. Otherwise, please enter your credit card information to gain access to our suite of mobile and web-based tools including Protect, the BrokerageDashboard, CurbCall Recruiting Matrix.")
    # display_alert_for_discount_alert
  end

  def display_alert_for_discount_alert
    
    rmq.app.alert(title: "Price Adjustment", message: "Do you have a pricing code?", style: :custom, fields:
               {user_name: {placeholder:"First Name"}, discount_id: {placeholder: 'Discount Code'}}, actions: :yes_no) do |action_type, fields|
      SVProgressHUD.showWithStatus("Checking..", maskType:SVProgressHUDMaskTypeGradient)
      case action_type
        when :yes
          puts "you entered '#{fields[:discount_id].text}'"
          App::Persistence['promo_code'] = fields[:discount_id].text
          data = BW::JSON.generate({ api_v1_braintree: {
                                       discount_id: "#{App::Persistence['promo_code']}",
                                     }
                                   })
          headers = { 'Content-Type' => 'application/json'}
          BW::HTTP.post(API.new.discounts, { headers: headers, payload: data} ) do |response|
            json = BW::JSON.parse(response.body.to_s)
            if response.ok?
              message = "Your discount is: " + "$" + "#{json[:status][:amount].to_i}"
              App::Persistence['amount_promo_code'] = json[:status][:amount]
            else
              App::Persistence['promo_code'] = nil
              message = json[:status]
            end
            SVProgressHUD.dismiss
            rmq.app.alert(title: 'Notification', message: message) {
              display_alert_for_bill_plans
            }
          end
        when :no
          display_alert_for_bill_plans
      end
      App::Persistence["#{App::Persistence['user_id']}-name"] = fields[:user_name].text
      update_name if valid_name
    end
  end
  
  def valid_name
    !User.name.nil? && User.name != ""
  end    
  
  def update_name
    SVProgressHUD.showWithStatus("Updating user name...", maskType:SVProgressHUDMaskTypeBlack)
    headers = { 'Authorization' => 'Token token="13ee4d1a3b6fe8cdaee7712f8cfe93caf5ef10b9"' }
    BW::HTTP.put(API.new.user, {payload: { user: {:name => User.name.capitalize} }, headers: headers}) do |response|
      p response
      SVProgressHUD.dismiss
    end
  end    

  def display_alert_for_bill_plans
    load_billing_plans
  end

  def display_error_for_missing_cc_details
    App.alert("We need your full credit card details.")
  end

  def load_billing_plans
    SVProgressHUD.showWithStatus("Checking..", maskType:SVProgressHUDMaskTypeGradient)
    payload = { promo_code: App::Persistence['promo_code']}
    BW::HTTP.get(API.new.bill_plans, {payload: payload}) do |response|
      json = BW::JSON.parse(response.body.to_str)
      puts json.inspect
      if response.ok?
        @plans = json
        if @plans.first['id'] == "CIQA"
          @plans.delete_at(1)
          display_alert_plans
        else
          @plans.delete_at(0)
          display_alert_plans
        end
      else
        App.alert('Error')
      end
    end
  end

  def display_alert_plans
    SVProgressHUD.dismiss

    rmq.app.alert title: "Your Price", message: "Tap to Confirm Price", style: :sheet, actions: @plans.map{ |p| "CurbCall Protect"+' '+"$#{+p["price_with_discount"].to_i}" } do |button_tag|
      @plans.each do |p|
          #if button_tag == p["name"]+' '"$#{+p["price_with_discount"].to_i}"
            App::Persistence['bill_plan_name'] = p["name"]
            App::Persistence['bill_plan'] = p["id"]
            App::Persistence['amount_bill_plan'] = p["price"]
            App::Persistence['price_with_discount'] = p["price_with_discount"]
          #end
      end
      if App::Persistence['promo_code'] == nil
        amount = App::Persistence['amount_bill_plan'].to_i
        message = "Your total amount due is " + "$" + amount.to_s
      else
        amount = App::Persistence['price_with_discount'].to_i
        message = "With your discount code, your total amount due is " + "$" + amount.to_s
      end
      App.alert(message)
    end
  end

  def tokenize_card(credit_card)
    puts "Tokenizing"
    cardClient = BTCardClient.alloc.initWithAPIClient(@scbt.client)
    card = BTCard.alloc.initWithNumber(credit_card.number, expirationMonth: credit_card.month, expirationYear: credit_card.year, cvv: credit_card.cvc)

    cardClient.tokenizeCard(card, completion: lambda do | tokenizedCard, error|
                            puts tokenizedCard.inspect
                            puts "error #{error.inspect}"
                            puts "nonce: #{tokenizedCard.nonce}"
                            App::Persistence['nonce'] = tokenizedCard.nonce
                            send_data_to_api(tokenizedCard.nonce, App::Persistence['promo_code'])

    end)
  end


  def send_data_to_api(nonce, discount)
    payload = {payment_method_nonce: nonce, discount_id: discount, email: User.email, first_name: User.first_name, last_name: User.last_name, plan_id: App::Persistence['bill_plan']}
    BW::HTTP.post(API.new.payment, {payload: payload}) do |response|
      begin
        json = BW::JSON.parse(response.body.to_s)
        if response.ok?
          App.alert(json[:status])
          open_root_screen HomeScreen.new(nav_bar:true)
        else
          App.alert(json[:status])
        end
      rescue Exception => e 
        App.alert("Looks like an invalid credit card.")      
      end        

    end
  end
end

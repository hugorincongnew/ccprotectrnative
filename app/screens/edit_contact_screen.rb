class EditContactScreen < PM::XLFormScreen
  title "Edit Contact"
  stylesheet EmergencyContactDetailScreenStylesheet
  include ContactFormValidator
  include RapidsosMethods
  form_options on_save: :save_it

  def on_load
    @contact = $contact
    #@contact = EmergencyContact.where(:name).eq(App::Persistence['contact']).first
  end

  def form_data
    [{
      title: "",
      cells: [
        {
          name: :name,
          title: "Contact First Name",
          type: :text,
          value: "#{@contact[:name]}",
        }, {
          name: :last_name,
          title: "Contact Last Name",
          type: :text,
          value: "#{@contact[:last_name]}",
        }, {
          name: :phone,
          title: "Phone Number",
          type: :text,
          value: "#{@contact[:phone]}"
        }, {
          name: :email,
          title: "Email",
          type: :text,
          value: "#{@contact[:email]}"
        }, {
          name: :submit,
          title: "Submit",
          type: :button,
          on_click: -> (cell) {
            on_save(nil)
          }
        }, {
          name: 'Delete',
          title: "Delete",
          type: :button,
          on_click: -> (cell) {
            destroy_contact
          }
        }
      ]
    }]
  end
  
  def clean_text(text)
    newtext = text.to_s.dup
    newtext.gsub(/[^a-zA-Z\-]/,"")
  end  
  
  def clean_number(num)
    newnum = num.to_s.dup
    newnum.gsub(/[^0-9\-]/,"")
  end     


  def save_it(values)
    mp on_save: data = values
    if valid_contact_info?(data)
      SVProgressHUD.showWithStatus("Updating contact...", maskType:SVProgressHUDMaskTypeBlack)
      headers = { 'Authorization' => 'Token token="13ee4d1a3b6fe8cdaee7712f8cfe93caf5ef10b9"' }
      @data = { user_token: App::Persistence['authToken'], emergency_contact:{name: clean_text(data['name']), email: data['email'], phone: clean_number(data['phone']), really_last_name: clean_text(data['last_name'])}}
      BW::HTTP.put(API.new.emergency_contact(@contact[:api_id]), {payload: @data, headers: headers}) do |response|       
        SVProgressHUD.dismiss
        if response.status_code == 200
          check_for_contacts(true)
          open_root_screen HomeScreen.new(nav_bar: false)
        else
          App.alert("Sorry something went wrong...")
        end
        SVProgressHUD.dismiss
      end
    end  
  end

  def destroy_contact
    rmq.app.alert(title: "Delete", message: "You sure?", actions: ['Delete', :cancel], style: :sheet) do |button_tag|
      if button_tag == 'Delete'
        headers = { 'Authorization' => 'Token token="13ee4d1a3b6fe8cdaee7712f8cfe93caf5ef10b9"' }
        BW::HTTP.delete(API.new.emergency_contact(@contact[:api_id]), { payload: @data, headers: headers }) do |response|
          p response
          check_for_contacts(true)
          SVProgressHUD.dismiss
          open_root_screen HomeScreen.new(nav_bar: false)
        end
      end
    end
  end

  def will_animate_rotate(orientation, duration)
    reapply_styles
  end
end

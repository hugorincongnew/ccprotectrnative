class EditProfileScreen < PM::Screen
  stylesheet EditProfileScreenStylesheet 
  include RapidsosMethods
  include ScreenSharedMethodsEdit
  
  def on_load
    add_items
    registerNotifications
  end
  
  def valid_form
    valid = true
    valid = false if @first_name.text.nil? or @first_name.text == ""
    valid = false if @last_name.text.nil? or @last_name.text == ""
    valid = false if @phonenumber.text.nil? or @phonenumber.text == ""
    
    if valid == false
      App.alert("Incomplete data", "Please set a valid First Name, Last Name and Phone Number")
    end  
    
    if @first_name.text.length >= 30
      App.alert("Invalid info", "First name shuld have less than 30 character")
      valid = false
    end
    
    if @last_name.text.length >= 30
      App.alert("Invalid info", "Last Name shuld have less than 30 character")
      valid = false
    end    
      
    return valid
  end    
  
  def clean_text(text)
    newtext = text.to_s.dup
    newtext.gsub(/[^a-zA-Z\-]/,"")
  end  
  
  def clean_number(num)
    newnum = num.to_s.dup
    newnum.gsub(/[^0-9\-]/,"")
  end    
  
  def add_items
    append!(UILabel, :edit_profile_title)
    @back_button = append!(UIButton, :back_arrow).on(:touch) { go_back } if App::Persistence["#{App::Persistence['user_id']}-no_subscription"] == false and !User.need_info?
    add_form
  end
  
  def add_form
    @first_name = rmq.append!(UITextField, :first_name_field)
    @first_name.setText(User.first_name) if User.first_name
    @first_name.setDelegate(self)

    @last_name = rmq.append!(UITextField, :last_name_field)
    @last_name.setText(User.last_name) if User.last_name
    @last_name.setDelegate(self)

    @phonenumber = rmq.append!(UITextField, :phonenumber_field)
    @phonenumber.setText(User.phone) if User.phone   
    @phonenumber.setDelegate(self)
     
    if App::Persistence["#{App::Persistence['user_id']}-no_subscription"] == true and !User.valid_promo?
      @promo = rmq.append!(UITextField, :promo_field)
      @promo.setDelegate(self)
    end
    
    rmq.append(UIButton, :update_button).on(:touch){|sender| update_and_close }
  end  
    
  def update_and_close
    if valid_form
      SVProgressHUD.showWithStatus("Updating your info...", maskType:SVProgressHUDMaskTypeBlack)
      headers = { 'Authorization' => 'Token token="13ee4d1a3b6fe8cdaee7712f8cfe93caf5ef10b9"' }
      BW::HTTP.put(API.new.user, {payload: { user: {:name => clean_text(@first_name.text), :really_last_name => clean_text(@last_name.text), :phone_number => clean_number(@phonenumber.text)} }, headers: headers}) do |response|
        App::Persistence['checked'] = true
        App::Persistence["#{App::Persistence['user_id']}-firstname"] = clean_text(@first_name.text)
        App::Persistence["#{App::Persistence['user_id']}-lastname"] = clean_text(@last_name.text)
        App::Persistence["#{App::Persistence['user_id']}-phone"] = clean_number(@phonenumber.text)
        if @promo
          if @promo.text.upcase == "TRIAL"
            App::Persistence["#{App::Persistence['user_id']}-no_subscription"] = false
            App::Persistence["#{App::Persistence['user_id']}-promo"] = "TRIAL"
            App::Persistence["#{App::Persistence['user_id']}-promo-date"] = Time.now.to_s
          end  
        end  
        if App::Persistence["#{App::Persistence['user_id']}-no_subscription"] == true
          if User.need_info?
            App.alert("Incomplete data", "Please set a valid First Name, Last Name and Phone Number")
          else
            if User.valid_promo?
              open PurchaseScreen.new(nav_bar:true)
            else
              open_root_screen HomeScreen.new(nav_bar: false) 
            end    
          end    
        else
          open_root_screen SettingsScreen.new(nav_bar: false) 
        end        
        SVProgressHUD.dismiss
      end
    else
      #
    end      
  end  
  
  def go_back
     open SettingsScreen.new(nav_bar: false)
  end    

end

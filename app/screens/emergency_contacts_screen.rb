class EmergencyContactsScreen < PM::TableScreen
  title "Emergency Contacts"
  stylesheet EmergencyContactsScreenStylesheet

  def on_load
    $ecs = self
    set_nav_bar_button :right, title: "New Contact", action: :new_contact
    set_nav_bar_button :left, title: "Home", action: :go_home
  end

  def table_data
    [{
      cells: $contacts.each.map do |ec|
        {
          title: "#{ec[:name]} #{ec[:last_name]}",
          action: :tapped_state,
          arguments: ec[:name],
          height: 150,
          # remote_image: {  # remote image, requires JMImageCache CocoaPod
          #   url: card.card_pic.to_s,
          #   placeholder: "icon-512", # NOTE: this is required!
          #   size: 150,
          #   radius: 0,
          #   content_mode: :scale_aspect_fill
          # }
        }
      end
    }]
  end

  def tapped_state(item)
    $contact = $contacts.select{|ec| ec[:name] == item.split(" ")[0]}.last
    App::Persistence['contact'] = item
    open EditContactScreen.new(nav_bar:true)
  end

  refreshable callback: :on_refresh,
    pull_message: "Pull to refresh",
    refreshing: "Refreshing data…",
    updated_format: "Last updated at %s",
    updated_time_format: "%l:%M %p"

  def on_refresh
    get_contacts
  end

  def get_contacts
    $contacts = []
    @data = {user_email: App::Persistence['user_email'], user_token: App::Persistence['authToken']}
    BW::HTTP.get(API.new.emergency_contacts, { payload: @data }) do |response|
      if response.ok?
        json = BW::JSON.parse(response.body.to_str)
        #EmergencyContact.each {|f| f.destroy}
        if json.count >= 1
          json.each do |contact| 
            $contacts << {api_id: contact['id'], name: contact['name'], phone: contact['phone'], email: contact['email'], last_name: contact['really_last_name'].to_s}
          end
          App::Persistence['contacts'] = $contacts
          end_refreshing
          update_table_data
        else
          App::Persistence['contacts'] = $contacts
          end_refreshing
          update_table_data
        end
      else
        App::Persistence.delete('auth_token')
      end
    end
  end

  def new_contact
    open NewContactScreen.new(nav_bar:true)
  end

  def go_home
    open_root_screen HomeScreen.new(nav_bar:false)
  end

  # Remove the following if you're only using portrait

  # You don't have to reapply styles to all UIViews, if you want to optimize, another way to do it
  # is tag the views you need to restyle in your stylesheet, then only reapply the tagged views, like so:
  #   def logo(st)
  #     st.frame = {t: 10, w: 200, h: 96}
  #     st.centered = :horizontal
  #     st.image = image.resource('logo')
  #     st.tag(:reapply_style)
  #   end
  #
  # Then in will_animate_rotate
  #   find(:reapply_style).reapply_styles#
  def will_animate_rotate(orientation, duration)
    reapply_styles
  end
end

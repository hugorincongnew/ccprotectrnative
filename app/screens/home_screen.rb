class HomeScreen < PM::Screen
  title "SafetyIQ"
  stylesheet HomeScreenStylesheet
  include RapidsosMethods
  
  def on_load
    $app2 = self
    #set_nav_bar_button :left, title:"Settings", action: :nav_left_button

    @begin_showing = append!(UIButton, :start_showing_button).on(:touch) { begin_showing }
    @logo_pic = append!(UIImageView, :logo_pic)
    @logo = append!(UIImageView, :logo)
    @grey_panic = append!(UIImageView, :grey_panic)
    @em_contacts_bar = append!(UIButton, :em_contacts_bar).on(:touch) { em_contacts }
    @logout_button = append!(UIButton, :logout_button).on(:touch) { logout }
    if Device.ios_version.to_f >= 8
      BW::Location.location_manager.requestAlwaysAuthorization
      #start_heartbeat
    else
      p "not iOS 8"
      #start_heartbeat
    end

    customer_payment
    
    if User.first_name.nil? or User.phone.nil?
      App.alert("Your profile info is incomplete.", maskType:SVProgressHUDMaskTypeGradient)
      open EditProfileScreen.new(nav_bar: false)
    end
    
    @setting_icon = append!(UIButton, :setting_icon_grey).on(:touch) { open SettingsScreen.new(nav_bar:false) }
    #auth_one_touch
  end 
  
  def on_appear
    auth_one_touch
  end  

  def nav_left_button
    mp 'Left button'
    open SettingsScreen.new(nav_bar:true)
  end

  def nav_right_button
    mp 'Right button'
  end

  def em_contacts
    app_delegate.get_contacts
  end

  def will_animate_rotate(orientation, duration)
    find.all.reapply_styles
  end

  def begin_showing
    p "starting the showing 2"
    App::Persistence['non_cc_showing'] = "true"
    #create showing though api
    headers = { 'Authorization' => 'Token token="13ee4d1a3b6fe8cdaee7712f8cfe93caf5ef10b9"' }
    @data = {user_email: App::Persistence['user_email'], user_token: App::Persistence['authToken'], agent_id: "#{App::Persistence['user_id']}", agent_name: "#{App::Persistence['user_name']}" }
    BW::HTTP.post("#{API_URL}/api/v1/users/#{App::Persistence['user_id']}/visits", { headers: headers, payload: @data}) do |response|
      p response
      if response.status_code == 201
        p json = BW::JSON.parse(response.body.to_str)
        App::Persistence['showing_id'] = json['id']
        open ShowingScreen.new
      else
        App.alert("oops, something went wrong. Please try that again...")
      end
    end
  end

  def logout
    EMGOneTouchManager.sharedInstance.logoutUser if EMGOneTouchManager.sharedInstance.isUserLoggedIn
    App::Persistence['user_name'] = nil
    headers = {
      'Content-Type' => 'application/json',
      'Authorization' => "Token token=\"#{App::Persistence['authToken']}\""
    }
    data = {user_email: App::Persistence['user_email'], user_token: App::Persistence['authToken']}
    BW::HTTP.delete(API.new.logout, { payload: data }) do |response|
      p response
      
      unmut = User.unmut
      App::Persistence.all.each do |k, v|   
        App::Persistence.delete(k) unless unmut.include?(k.split("-")[1])
      end
      App::Persistence['authToken'] = nil
      #app_delegate.unregister_for_push_notifications
      App::Persistence['logged_in'] = false
      open WelcomeScreen.new
      #app_delegate.get_regions
    end
  end

  def customer_payment
    headers = {
      'Content-Type' => 'application/json',
      'Authorization' => "Token token=\"#{App::Persistence['authToken']}\""
    }
    BW::HTTP.get(API.new.user_payment, { headers: headers, payload: {:ios => true}}) do |response|
      json = BW::JSON.parse(response.body.to_str)
      
      mp json
      if json == []
        back_to_payment unless User.valid_promo?  
      else
        if json.nil? or json == "" or json == []
          back_to_payment
        else
          asubscription = active_subscription(json)
          if asubscription
            if expired?(asubscription)
              
              back_to_payment unless User.valid_promo?       
            else
              App::Persistence["#{App::Persistence['user_id']}-no_subscription"] = false
              App::Persistence['checked'] = true
            end  
          else
             back_to_payment unless User.valid_promo?     
          end    
        end  
      end  
      SVProgressHUD.dismiss
    end
  end
  
  def active_subscription(subscriptions)
    subscriptions.select{|s| s["current_state"] == "actived"}.last
  end  
  
  def expired?(subscription)
    mp "selected"
    mp subscription
    mp "selected"    
    if subscription["expired_date"]
      
      dateFormatter = NSDateFormatter.alloc.init
      dateFormatter.setDateFormat("yyyy-MM-dd")
      date = dateFormatter.dateFromString(subscription["expired_date"][0..9])
      now = NSDate.new
      return true if now > date  
      false
    else
      true
    end    
  end    

  def back_to_payment
    SVProgressHUD.dismiss
    open PurchaseScreen.new(nav_bar:true)
    #open CreditCardEntry.new(nav_bar:true)
  end

end

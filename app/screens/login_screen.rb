class LoginScreen < PM::Screen
  title "Login"
  nav_bar true , {
    toolbar: true,
    hide_nav_bar: false
  }

  include ScreenSharedMethods
  stylesheet LoginScreenStylesheet

  def on_load
    registerNotifications
    @logo_pic = append!(UIImageView, :logo_pic)
    @logo = append!(UIImageView, :logo)
    add_form
  end

  def add_form
    rmq.append(UIButton, :login).on(:touch){|sender| send_data_to_api }
    @login = rmq.append!(UITextField, :text_login)
    @login.setKeyboardType(UIKeyboardTypeEmailAddress)
    @login.setDelegate(self)

    rmq.append(UIButton, :lost_password).on(:touch){|sender| load_forgot_password }
    rmq.append(UILabel, :label_style)
    rmq.append(UILabel, :text_in_label)
    rmq.append(UIButton, :sign_up).on(:touch){|sender| open_register }

    @password = rmq.append!(UITextField, :text_password)
    @password.setDelegate(self)
  end

  def open_register
    open RegisterScreen.new(navbar:true)
  end

  def user_data
    {username: @login.text, password: @password.text }
  end

  def send_data_to_api
    api_call
  end

  def load_forgot_password
    open ForgotPasswordScreen.new(nav_bar: true, external_links: false)
  end

  def display_agree_to_terms
    UIAlertView.alert("Terms and Privacy", buttons: ["I Agree", "Read Terms", "Read Privacy"],
          message: "Do you agree to the Terms and Conditions and Privacy Policy?") { |button|
          if button == "Read Privacy"
            privacy_policy
          elsif button == "Read Terms"
            terms_of_service
          elsif button == "I Agree"
            api_call
          end
    }

  end


  def api_call
    SVProgressHUD.showWithStatus("Logging in", maskType:SVProgressHUDMaskTypeGradient)

    headers = { 'Content-Type' => 'application/json'}
    data = BW::JSON.generate({ api_v1_user: {
                                 email: @login.text.downcase,
                                 password: @password.text,
                                 dev_token: "#{App::Persistence['device_token']}"
                                } })



    BW::HTTP.post(SESSIONS_URL, { headers: headers, payload: data } ) do |response|

      if response.status_code == 422
        App.alert("There was a problem with your request. Maybe adding the correct login or password")
      elsif response.status_description.nil?
        App.alert(response.error_message)
      else
        if response.ok?
            json = BW::JSON.parse(response.body.to_str)
            save_to_local_db(json)
            open_root_screen HomeScreen.new(nav_bar: false)
        elsif response.status_code.to_s =~ /40\d/
          App.alert("Login failed")
        else
          App.alert(response.to_str)
        end
      end
      SVProgressHUD.dismiss
    end

  end

  def will_animate_rotate(orientation, duration)
    reapply_styles
  end

  def terms_of_service
    App.open_url("http://www.curbcall.com/tos/")
  end

  def privacy_policy
    App.open_url("http://www.curbcall.com/privacy/")
  end

  def save_to_local_db(json)
    #puts json.inspect
    App::Persistence['user_id'] = json['id']
    App::Persistence['authToken'] = json['auth_token']
    App::Persistence['user_name'] = json['name']
    App::Persistence["#{App::Persistence['user_id']}-firstname"] = json['name']
    App::Persistence["#{App::Persistence['user_id']}-lastname"] = json['really_last_name']
    App::Persistence["#{App::Persistence['user_id']}-phone"] = json['phone_number']      
    App::Persistence['user_email'] = json['email']

    App::Persistence['bt_id'] = json['bt_customer_id']
    App::Persistence['user_agent'] = true
    App::Persistence['logged_in'] = true
    #currentInstallation = PFInstallation.currentInstallation
    #currentInstallation.setDeviceTokenFromData(App::Persistence['device_token'])
    #currentInstallation.setObject(App::Persistence['user_id'].to_i, forKey:"user_id")
    #currentInstallation.saveInBackground
  end

end

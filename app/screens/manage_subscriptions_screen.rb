class ManageSubscriptionsScreen < PM::Screen
  stylesheet ManageSubscriptionsScreenStylesheet 
  include RapidsosMethods
    
  def on_load
    add_items
    get_subscription
  end
  
  def get_subscription
    SVProgressHUD.showWithStatus("Getting Subscription info...", maskType:SVProgressHUDMaskTypeGradient)
    subscriptions = []
    headers = {'Content-Type' => 'application/json', 'Authorization' => "Token token=\"#{App::Persistence['authToken']}\"" }
    BW::HTTP.get(API.new.user_payment, { headers: headers}) do |response|
      if response.ok?
        json = BW::JSON.parse(response.body.to_str)
        subscriptions = json
        display_subscriptions(subscriptions)
      end
      SVProgressHUD.dismiss
    end
  end  
  
  def display_subscriptions(subscriptions)
    s = subscriptions.last
    unless subscriptions.nil? or subscriptions == "" or subscriptions == []
      mp s
    else
      open PurchaseScreen.new(nav_bar: true)
    end  
  end  
  
  def add_items
    append!(UILabel, :subcriptions_title)    
    @back_button = append!(UIButton, :back_arrow).on(:touch) { go_back }     
  end
  
  def go_back
     open SettingsScreen.new(nav_bar: false)
  end    

end

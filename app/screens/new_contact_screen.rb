class NewContactScreen < PM::XLFormScreen
  title "New Contact"
  stylesheet EmergencyContactDetailScreenStylesheet
  include ContactFormValidator
  form_options on_save: :save_it

  def on_load
    @contact = EmergencyContact.where(:name).eq(App::Persistence['contact']).first
  end

  def form_data
    [{
      title: "",
      cells: [
        {
          name: "name",
          title: "Contact First Name",
          type: :text,
          value: "",
        }, {
          name: "last_name",
          title: "Contact Last Name",
          type: :text,
          value: "",
        }, {
          name: "phone",
          title: "Phone Number",
          type: :number,
          value: ""
        }, {
          name: "email",
          title: "Email",
          type: :text,
          value: ""
        }, {
          name: :submit,
          title: "Submit",
          type: :button,
          on_click: -> (cell) {
            on_save(nil)
          }
        }
      ]
    }]
  end


  def clean_text(text)
    newtext = text.to_s.dup
    newtext.gsub(/[^a-zA-Z\-]/,"")
  end  
  
  def clean_number(num)
    newnum = num.to_s.dup
    newnum.gsub(/[^0-9\-]/,"")
  end     


  def save_it(values)
    mp on_save: data = values
    
    if valid_contact_info?(data)
      SVProgressHUD.showWithStatus("Adding new contact...", maskType:SVProgressHUDMaskTypeBlack)

      phone = data['phone'].to_i
      phone = "#{phone}"
      # create locally
      
      #send it to the server
      headers = { 'Authorization' => "Token token=\"#{App::Persistence['authToken']}\"" }
      @data = {emergency_contact:{name: clean_text(data['name']), email: data['email'], phone: clean_number(phone), really_last_name: clean_text(data['last_name'])}}
      BW::HTTP.post("#{API_URL}/api/v1/users/#{App::Persistence['user_id']}/emergency_contacts", {payload: @data, headers: headers}) do |response|
        p response
        if response.status_code == 201
          SVProgressHUD.dismiss
          EM.add_timer 1.0 do
            open_root_screen HomeScreen.new(nav_bar: false)
          end  
        else
          SVProgressHUD.dismiss
          App.alert("Sorry something went wrong...")
        end
      end
    end
 end
 
end

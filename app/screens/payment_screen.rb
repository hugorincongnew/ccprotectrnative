class PaymentScreen < PM::XLFormScreen
  title "Payment"
  form_options on_save: :submit_form

  def on_load
    load_braintree_client
  end

  def load_braintree_client
    @scbt = SC::BrainTree.new.load_braintree_client
  end


  def form_data
    Forms::CreditCard.data
  end

  def tokenize_card(credit_card)

    cardClient = BTCardClient.alloc.initWithAPIClient(@scbt.client)
    card = BTCard.alloc.initWithNumber(credit_card.number, expirationMonth: credit_card.month, expirationYear: credit_card.year, cvv: credit_card.cvc)

    cardClient.tokenizeCard(card, completion: lambda do | tokenizedCard, error|
                            puts tokenizedCard.inspect
                            puts "error #{error.inspect}"
                            puts "nonce: #{tokenizedCard.nonce}"
                            send_data_to_api(tokenizedCard.nonce, credit_card.discount)
    end)
  end

  def submit_form(values)
    @credit_card = CreditCard.new(data['card_number'], data['exp_month'], data['exp_year'], data['cvc'], data['discount_id'])

    if valid_cc_params?(data)
      tokenize_card(@credit_card) 
    else
      display_error_for_missing_cc_details
    end
  end

  def valid_cc_params?(data)
    (data['card_number'] != "" &&  data['exp_month'] != "" && data['exp_year'] != "" && data['cvc'] != "" )
  end

  def display_error_for_missing_cc_details
    App.alert("We need your full credit card details.")
  end


  def send_data_to_api(nonce, discount)
    payload = {}

    BW::HTTP.post(API.new.user_payment, {payload: payload}) do |response|
      json = BW::JSON.parse(response.body.to_str)
      puts json.inspect
      puts "Terminando"
      if response.ok?
        App.alert(json[:status])
        open_root_screen HomeScreen.new(nav_bar:true)
      else
        App.alert(json[:status])
      end
    end
  end
end

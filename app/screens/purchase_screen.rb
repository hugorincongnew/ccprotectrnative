class PurchaseScreen < PM::TableScreen
  title "Subscriptions"
  stylesheet PurchaseScreenStylesheet
  include PM::IAP
  
    
  def on_load
    set_nav_bar_button :right, title: "Logout", action: :logout
    set_nav_bar_button :left, title: "Promo Code", action: :set_promo   
    @products = []
    if User.first_name.nil? or User.phone.nil?
      App.alert("Your profile info is incomplete.", maskType:SVProgressHUDMaskTypeGradient)
      open EditProfileScreen.new(nav_bar: false)
    end

    if User.valid_promo?
      open_root_screen HomeScreen.new(nav_bar: false) 
    else  
     App::Persistence["#{App::Persistence['user_id']}-no_subscription"] = true
     SVProgressHUD.showWithStatus("Please Wait, Getting info from appstore.", maskType:SVProgressHUDMaskTypeBlack)  
    get_iap 
    end  
  end
  
  def set_promo
    open_root_screen EditProfileScreen.new(nav_bar: false)
  end  
  
  def logout
    App::Persistence['user_name'] = nil
    headers = {
      'Content-Type' => 'application/json',
      'Authorization' => "Token token=\"#{App::Persistence['authToken']}\""
    }
    data = {user_email: App::Persistence['user_email'], user_token: App::Persistence['authToken']}
    BW::HTTP.delete(API.new.logout, { payload: data }) do |response|
      p response
      
      App::Persistence.all.each do |k, v|        
        App::Persistence.delete(k) unless included_in?(k) 
      end
      App::Persistence['authToken'] = nil
      #app_delegate.unregister_for_push_notifications
      App::Persistence['logged_in'] = false
      open WelcomeScreen.new

    end
  end  
  
  
  def included_in?(k)
    included = false
    User.unmut.each do |u|
      if included == false
        included = true if k.include?(u)
      end  
    end
    included
  end   
  

  def get_iap
    retrieve_iaps [ "com.curbcall.protect.1month", "com.curbcall.protect.1year" ] do |products, error|
      @products = products
      SVProgressHUD.dismiss
      update_table_data
    end
  end
  
  def update_tabl
    mp @products.products
    SVProgressHUD.dismiss
    update_table_data
  end  

  def table_data
    if @products.nil?
      []
    else
      [{ title:"", cells: product_cells }]
    end
  end
  
  def product_cells
    mp @products.inspect
    @products.map do |product| 
      {
        title: product[:title], 
        subtitle:"#{product[:formatted_price]}", 
        height:60, 
        action: :tapped_cell, 
        arguments: { product_id: product[:product_id] },
        properties: {
          background_color: UIColor.yellowColor
        } 
      }
    end  
  end  

  def tapped_cell(args={})
    #return complete_transaction
    mp "tapped!"
    
    purchase_iaps [ args[:product_id] ], username: User.name do |status, transaction|
      case status
       when :in_progress
         # Usually do nothing, maybe a spinner
       when :deferred
         # Waiting on a prompt to the user
       when :purchased
         complete_transaction(args[:product_id])
       when :canceled
         # They just canceled, no big deal.
       when :error
         # Failed to purchase
         App.alert("Something Went Wrong with the buy...")
         #transaction.error.localizedDescription # => error message
       end
    end   
  end

  def check_if_bought
    timer = EM.add_periodic_timer 2.0 do
      @iap.restore do |status, data|
        if status == :restored
          EM.cancel_timer(timer)
          complete_transaction
        else
          p "not done yet..."
        end
      end
    end
  end
  
  def get_exp_date(plan_id)
    if plan_id == "com.curbcall.protect.1month"
      30.days.after(NSDate.new)
    elsif plan_id == "com.curbcall.protect.1year"
      365.days.after(NSDate.new)
    end  
  end  

  def complete_transaction(plan_id)
    mp "complete transaction"
    
    payload = {user_id: App::Persistence['user_id'], plan_name: plan_id, expired_date: get_exp_date(plan_id)}
    BW::HTTP.post(API.new.iap_subscription, {payload: payload}) do |response|
      json = BW::JSON.parse(response.body.to_str)
      puts json.inspect
      puts "Terminando"
      if response.ok?
        App::Persistence["#{App::Persistence['user_id']}-no_subscription"] = false
        App::Persistence["#{App::Persistence['user_id']}-subscribed"] = true
        open_root_screen HomeScreen.new(nav_bar: false)
      else
        App.alert("Something Went Wrong...")
      end
    end
  end

  def will_animate_rotate(orientation, duration)
    reapply_styles
  end
end

class RegisterScreen < PM::Screen

  title "Sign Up!"
  stylesheet RegisterScreenStylesheet
  include ScreenSharedMethods

  def on_load
    registerNotifications
    set_nav_bar_button :left, title: "Back", action: :welcome
    @logo_pic = append!(UIImageView, :logo_pic)
    @logo = append!(UIImageView, :logo)
    rmq.append(UILabel, :label_style)
    rmq.append(UILabel, :text_in_label)
    rmq.append(UIButton, :log_in).on(:touch){|sender| open_login }
    add_form
  end

  def add_form
    rmq.append(UIButton, :login).on(:touch){|sender| send_data_to_api }
    @login = rmq.append!(UITextField, :text_login)
    @login.setKeyboardType(UIKeyboardTypeEmailAddress)
    @login.delegate = self

    @password = rmq.append!(UITextField, :text_password)
    @password.delegate = self
  end

  def open_login
    open LoginScreen.new(navbar:true)
  end

  def send_data_to_api

    @data = BW::JSON.generate({ api_v1_user: {
                                 email: @login.text.downcase,
                                 password: @password.text,
                                 password_confirmation: @password.text
                                } })

    App::Persistence['email'] = @login.text.downcase

    terms_and_policy
  end

  def terms_and_policy
    rmq.app.alert(title: "Terms and Conditions", message: "I agree to the Curb Call Terms of services and the Privacy Policy.", style: :sheet, actions: ["Terms", "Privacy Policy", "Agree", :cancel]) do |button_tag|
      case button_tag
      when "Terms"
        open TermsWebScreen.new(nav_bar: true, external_links: false)
      when "Privacy Policy"
        open PrivacyPolicyWebScreen.new(nav_bar: true, external_links: false)
      when "Agree"
        App::Persistence['terms'] = true
        registration
      when :cancel
        open RegisterScreen.new
      end
    end
  end

  def registration
    headers = { 'Content-Type' => 'application/json'}
    BW::HTTP.post(API.new.registration, { headers: headers, payload: @data} ) do |response|
      if response.status_code == 422
        App.alert("Looks like this user email exists!")
      elsif response.status_description.nil?
        App.alert(response.error_message)
      else
        if response.ok?
          json = BW::JSON.parse(response.body.to_str)
          puts json.inspect
          save_to_local_db(json)
          #open CreditCardEntry.new(navbar:true)
          open_root_screen HomeScreen.new(navbar:true)
        elsif response.status_code.to_s =~ /40\d/
          App.alert("Login failed")
        else
          App.alert(response.body.to_str)
        end
      end
    end
  end

  def save_to_local_db(json)
    p json.inspect
    p "saving to local db: #{json}"
    App::Persistence['auth_token'] = json["auth_token"]
    App::Persistence['user_id'] = json["id"]
    App::Persistence['user_name'] = json["name"]
    App::Persistence['user_email'] = json["email"]
    App::Persistence['logged_in'] = true
    currentInstallation = PFInstallation.currentInstallation
    currentInstallation.setDeviceTokenFromData(App::Persistence['push_token'])
    currentInstallation.setObject(App::Persistence['user_id'].to_i, forKey:"user_id")
    currentInstallation.saveInBackground
  end

  def will_animate_rotate(orientation, duration)
    reapply_styles
  end

  def welcome
    open WelcomeScreen.new
  end


end

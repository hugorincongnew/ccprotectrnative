class SettingsScreen < PM::Screen
  stylesheet SettingScreenStylesheet 
  include RapidsosMethods
    
  def on_load
    add_items
  end
  
  def add_items
    @profile_button = append!(UIButton, :edit_profile_button).on(:touch) { go_to_edit_profile }
    @manage_subscriptions_button = append!(UIButton, :manage_subscriptions_button).on(:touch) { go_to_manage_subscriptions }    
    @emergency_contacts_button = append!(UIButton, :emergency_contacts_button).on(:touch) { go_to_ec }
    @logout_button = append!(UIButton, :logout_button).on(:touch) { logout }
    @back_button = append!(UIButton, :back_arrow).on(:touch) { go_back }    
  end
  
  def go_to_manage_subscriptions
    open SubscriptionsScreen.new(nav_bar: true)
  end  
  
  def go_to_edit_profile
    open EditProfileScreen.new(nav_bar: false)
  end
  
  def go_to_ec
    app_delegate.get_contacts
  end  
  
  def go_back
    open HomeScreen.new(nav_bar: false)
  end    

  def logout
    headers = {
      'Content-Type' => 'application/json',
      'Authorization' => "Token token=\"#{App::Persistence['authToken']}\""
    }
    data = {user_email: App::Persistence['user_email'], user_token: App::Persistence['authToken']}
    BW::HTTP.delete(API.new.logout, { payload: data }) do |response|
      p response
      App::Persistence.all.each do |k, v|        
        App::Persistence.delete(k) unless included_in?(k) 
      end
      App::Persistence['authToken'] = nil
      App::Persistence['logged_in'] = false
      #app_delegate.unregister_for_push_notifications
      open WelcomeScreen.new
      #app_delegate.get_regions
    end
  end
  
  def included_in?(k)
    included = false
    User.unmut.each do |u|
      if included == false
        included = true if k.include?(u)
      end  
    end
    included
  end  

  def contacts
    open EmergencyContactsScreen.new(nav_bar:true)
    #app_delegate.get_contacts
  end

  def go_to_update_credit_card
    open  UpdateCreditCardScreen.new
  end

  def will_animate_rotate(orientation, duration)
    reapply_styles
  end
end

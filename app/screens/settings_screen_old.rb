class SettingsScreenOld < PM::TableScreen
  title "Settings"
  #stylesheet SettingsScreenStylesheet

  def on_load
  end

  def table_data
    [{
      title: "Options and Settings",
      cells: [
        { title: "Emergency Contacts", action: :contacts },
        { title: "Update Billing", action: :go_to_update_credit_card },
        { title: "Logout", action: :logout }
      ]
    }]
  end

  def logout
    headers = {
      'Content-Type' => 'application/json',
      'Authorization' => "Token token=\"#{App::Persistence['authToken']}\""
    }
    data = {user_email: App::Persistence['user_email'], user_token: App::Persistence['authToken']}
    BW::HTTP.delete(API.new.logout, { payload: data }) do |response|
      p response
      unmut = User.unmut
      App::Persistence.all.each do |k, v|   
        App::Persistence.delete(k) unless unmut.include?(k)
      end
      App::Persistence['authToken'] = nil
      App::Persistence['logged_in'] = false
      app_delegate.unregister_for_push_notifications
      open WelcomeScreen.new
      #app_delegate.get_regions
    end
  end

  def contacts
    open EmergencyContactsScreen.new(nav_bar:true)
    #app_delegate.get_contacts
  end

  def go_to_update_credit_card
    open  UpdateCreditCardScreen.new
  end

  def will_animate_rotate(orientation, duration)
    reapply_styles
  end
end

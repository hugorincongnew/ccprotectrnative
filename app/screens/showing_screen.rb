class ShowingScreen < PM::Screen
  include SugarCube::Timer
  include RapidsosMethods
  title "On a Showing"
  stylesheet ShowingScreenStylesheet

  def on_load
    $app2 = self
    set_nav_bar_button :left, title:" ", action: :nav_left_button

    @end_showing = append!(UIButton, :end_showing_button).on(:touch) { end_showing }
    @logo_pic = append!(UIImageView, :logo_pic)
    #@logo_pic = append!(UIButton, :logo_pic).on(:touch) { udatex }
    
    @red_panic = rmq.append(UIImageView, :panic_button_style)
    @red_panic_obj = @red_panic.get
    @red_panic_obj.userInteractionEnabled = true
    @red_panic_obj.on_press(1) do |gesture|
      if gesture.state == UIGestureRecognizerStateBegan
        panic_questions
      end  
    end  
    
    start_heartbeat
    auth_one_touch
  end
  
  def udatex
    @manager.update!
  end  


  def start_heartbeat
     #on location ping
     i=0
     @manager = Locman::Manager.new(
       accuracy: :ten_meters,
       distance_filter: 10, # in meter
       background: true
     )

     @manager.after_authorize = lambda do |authorized|
       puts "Authorized!" if authorized
     end

     @manager.authorize!

     @manager.on_update = lambda do |locations|
       locations.each do |loc|
         #App.alert("#{loc.latitude}, #{loc.longitude}") # DEBUG
         i = i+1
           #if (i%5)==0
             headers = { 'Authorization' => 'Token token="13ee4d1a3b6fe8cdaee7712f8cfe93caf5ef10b9"' }
             @data = {visit_id:App::Persistence['showing_id'], lat: loc.latitude, lon: loc.longitude}
             #AFMotion::JSON.post("#{API_URL}/api/v1/visits/#{App::Persistence['showing_id']}/sightings", @data) do |result|
             BW::HTTP.post("#{API_URL}/api/v1/visits/#{App::Persistence['showing_id']}/sightings", { headers: headers, payload: @data}) do |response|
               p "Beat #{response}"
             end # post
           #end # if i%_
       end
     end
     begin
       @manager.update!
     rescue Exception => e 
       end_showing
       App.alert("We can not get your current location, please check your Device Settings > CC Protect and set location on 'Always'")      
     end     
   end

   def end_heartbeat
     @manager.stop_update!
   end

   def warning
     p "Triggering Warning Mode"
     headers = { 'Authorization' => 'Token token="13ee4d1a3b6fe8cdaee7712f8cfe93caf5ef10b9"' }
     @data = { visit:{state:"warning"}}
     BW::HTTP.put("#{API_URL}/api/v1/visits/#{App::Persistence['showing_id']}", {headers: headers, payload: @data}) do |response|
       p response
       panic
     end
   end

   def panic
     mp "triggering panic mode"
     #headers = { 'Authorization' => 'Token token="13ee4d1a3b6fe8cdaee7712f8cfe93caf5ef10b9"' }
     #@data = { visit:{state:"danger"}}
     #BW::HTTP.put("#{API_URL}/api/v1/visits/#{App::Persistence['showing_id']}", {headers: headers, payload: @data}) do |response|
     #   p response
     #end
     #
     
     rapidSOSPanic
   end
   
   def rapidSOSPanic
     mp "[rapidSOS] - Calling Panic"
     EMGOneTouchManager.sharedInstance.createOneTouchWaterfallAlertWithCompletion(proc{|success, error| 
         if !success
           NSLog("[rapidSOS] - Error: #{error.localizedDescription}")
         end
         mp "[rapidSOS] - Panic Success"
         App.alert("Contacts Notified", message: "We've notified your emergency contact.")
       }, callFailed:proc{|error| 
         #proxy call failed to come in after 20 seconds - call callFailsafe method to go to native dialer and handle anything else
         EMGOneTouchManager.sharedInstance.callFailsafe
       }
     )  
   end 

   def panic_questions
     panic
     
     #UIAlertView.alert("Are you in trouble?", buttons: ["No", "Yes"],
     #     message: "Should we notify your emergency contact?") { |button|
     #       if button == "No"
     #
     #       elsif button == "Yes"
     #         
     #      end
     #}
   end

   def end_showing
     p "ending the showing"
     App::Persistence['non_cc_showing'] = "false"
     end_heartbeat
     headers = { 'Authorization' => 'Token token="13ee4d1a3b6fe8cdaee7712f8cfe93caf5ef10b9"' }
     @data = { status: "completed", user_email: App::Persistence['user_email'], user_token: App::Persistence['authToken'] }
     BW::HTTP.post("#{API_URL}/api/v1/visits/#{App::Persistence['showing_id']}/end", {headers: headers}) do |response|
       p response
     end

     open_root_screen HomeScreen.new(navbar:true)
   end

  def will_animate_rotate(orientation, duration)
    reapply_styles
  end
end

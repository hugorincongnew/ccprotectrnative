class SubscriptionsScreen < PM::TableScreen
  title "Subscriptions"
  #refreshable
  stylesheet SettingScreenStylesheet 
  row_height :auto, estimated: 80
    
  def on_load
    set_nav_bar_button :left, title: "Settings", action: :go_settings
    @subscriptions = []
    load_async
  end

  def table_data
    [{
      cells: @subscriptions.map do |subscription|
        {
          title: subscription["plan_id"] == "com.curbcall.protect.1year" ? "Curbcall Protect 1 Year" : "Curbcall Protect 1 Month",
          subtitle: subscription_subtitle(subscription),
          action: :cancel_subscription_question,
          arguments: { subscription: subscription }
        }
      end
    }]
  end  
  
  def subscription_subtitle(subscription)
    if subscription["current_state"] == "actived"
      "#{subscription["current_state"]}, Expires on: #{expired_date(subscription)}"
    else
      subscription["current_state"]
    end    
  end  
  
  def expired_date(subscription)
    dateFormatter = NSDateFormatter.alloc.init
    dateFormatter.setDateFormat("yyyy-MM-dd")
    date = dateFormatter.dateFromString(subscription["expired_date"][0..9])
  
    date.string_with_format('MMMM dd, YYYY')
  end  
  
  def cancel_subscription_question(args={})
    if args[:subscription]["current_state"] == "actived"
      UIAlertView.alert("Attention!", buttons: ["Yes", "Cancel"],
          message: "Are you sure to cancel your current subscription?") { |button|
          if button == "Yes"
            cancel_subscription(args[:subscription])
          end
      }
    end  
  end  
  
  def cancel_subscription(subscription)
    SVProgressHUD.showWithStatus("Canceling Subscription", maskType:SVProgressHUDMaskTypeBlack)
    headers = {
      'Content-Type' => 'application/json',
      'Authorization' => "Token token=\"#{App::Persistence['authToken']}\""
    }
    BW::HTTP.delete(API.new.subscriptions(subscription["id"]), { headers: headers}) do |response|
      SVProgressHUD.dismiss
      open_root_screen HomeScreen.new(nav_bar: false)
    end   
  end  

  def go_settings
    open_root_screen SettingsScreen.new(nav_bar:false)
  end  

  def on_refresh
    load_async
  end

  def load_async
    headers = {
      'Content-Type' => 'application/json',
      'Authorization' => "Token token=\"#{App::Persistence['authToken']}\""
    }
    BW::HTTP.get(API.new.user_payment, { headers: headers, payload: {:ios => true}}) do |response|
      json = BW::JSON.parse(response.body.to_str)
      
      mp json
      if json == []
        @subscriptions = []
      else
        if json.nil? or json == "" or json == []
          @subscriptions = []
        else
          @subscriptions = json.sort! { |a,b| b["id"] <=> a["id"] }
        end  
      end  
      stop_refreshing
      update_table_data 
    end       
  end

end

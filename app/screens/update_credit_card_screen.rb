class UpdateCreditCardScreen < PM::XLFormScreen
  form_options on_save:   :submit_form

def on_load
	@scbt = SC::BrainTree.new.load_braintree_client
	puts "nonce #{App::Persistence['nonce']}"
end

  def form_data
    [{
      title: "",
      footer: "",
      cells: [{
      name: "card_number",
      title: "Card Number",
      type: :password,
      value: "4111111111111111"
    },
      {
      name: "exp_month",
      title: "Expiration Month",
      value: "",
      type: :selector_push,
      options: {
      "01" => "01",
      "02" => "02",
      "03" => "03",
      "04" => "04",
      "05" => "05",
      "06" => "06",
      "07" => "07",
      "08" => "08",
      "09" => "09",
      "10" => "10",
      "11" => "11",
      "12" => "12"
    },
    }, {
      name: "exp_year",
      title: "Expiration Year",
      value: "",
      type: :selector_push,
      options: {
    	"2016" =>  "16",
      	"2017" =>  "17",
        "2018" =>  "18",
        "2019" =>  "19",
        "2020" =>  "20",
    	  "2021" =>  "21",
		    "2022" =>  "22",
        "2023" =>  "23",
        "2024" =>  "24",
        "2025" =>  "25",
        "2026" =>  "26",
        "2027" =>  "27"
    },
    }, {
      name: "cvc",
      title: "CVC Code",
      type: :password,
      value: ""
    },{
      name: :submit,
      title: "Submit",
      type: :button,
      appearance: {
      font: UIFont.fontWithName('Helvetica Neue', size: 15.0),
      detail_font: UIFont.fontWithName('Helvetica Neue', size: 12.0),
      color: UIColor.whiteColor,
      etail_color: UIColor.whiteColor,
      background_color: UIColor.blueColor
    },
      on_click: -> (cell) {
      on_save(nil)
    }

    }]
    }]
  end

  def submit_form(data)
    puts "Saving it"
    @credit_card = CreditCard.new(data['card_number'], data['exp_month'], data['exp_year'], data['cvc'], App::Persistence['promo_code'])
    puts @credit_card.inspect
    if valid_cc_params?(data)
      tokenize_card(@credit_card)
    else
      display_error_for_missing_cc_details
    end
  end

  def display_error_for_missing_cc_details
    App.alert("We need your full credit card details.")
  end

  def valid_cc_params?(data)
    (data['card_number'] != "" &&  data['exp_month'] != "" && data['exp_year'] != "" && data['cvc'] != "" )
  end

   def tokenize_card(credit_card)
    puts "Tokenizing"
    cardClient = BTCardClient.alloc.initWithAPIClient(@scbt.client)
    card = BTCard.alloc.initWithNumber(credit_card.number, expirationMonth: credit_card.month, expirationYear: credit_card.year, cvv: credit_card.cvc)

    cardClient.tokenizeCard(card, completion: lambda do | tokenizedCard, error|
                            puts tokenizedCard.inspect
                            puts "error #{error.inspect}"
                            puts "nonce: #{tokenizedCard.nonce}"
                            App::Persistence['nonce'] = tokenizedCard.nonce
                            update_credit_card_on_api(tokenizedCard.nonce)




    end)
  end

  def update_credit_card_on_api(nonce)
  	headers = { 'Content-Type' => 'application/json'}
  	payload = BW::JSON.generate({:payment_method_nonce => nonce, :user_email => App::Persistence['user_email']})
  	BW::HTTP.post(API.new.create_credit_card, { headers: headers, payload: payload} ) do |response|
    	json = BW::JSON.parse(response.body.to_str)
        if response.ok?
        	App.alert("Credit Card Updated Successfully")
        	open_root_screen HomeScreen.new(nav_bar: true)
        else
          error_message = json[:bt_error] ? json[:bt_error] : json[:status]
          App.alert(error_message )
        end
    end
  end


end

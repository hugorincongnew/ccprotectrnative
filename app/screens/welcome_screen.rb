class WelcomeScreen < PM::Screen
  title "CC Protect"
  stylesheet WelcomeScreenStylesheet

  def on_load
    EMGOneTouchManager.sharedInstance.logoutUser if EMGOneTouchManager.sharedInstance.isUserLoggedIn
    
    App::Persistence['rsosauth'] = false
    App::Persistence['user_name'] = nil
    @logo_pic = append!(UIImageView, :logo_pic)
    @logo = append!(UIImageView, :logo)
    @login_button = append!(UIButton, :login).on(:touch) { open_login }
    @register_buttom = append!(UIButton, :register).on(:touch) { open_register }
  end

  def open_login
    open LoginScreen.new(nav_bar: false)
  end

  def open_register
    open RegisterScreen.new(nav_bar: false)
  end

end

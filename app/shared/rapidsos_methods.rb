module RapidsosMethods
  
  def auth_one_touch(forced=false)

    if !EMGOneTouchManager.sharedInstance.isUserLoggedIn and !User.need_info?
      mp "auth in"  
      mp "phone: #{User.phone.to_s}"
      mp "first name: #{User.first_name}"
      mp "last name: #{User.last_name}"
      
      EMGOneTouchManager.sharedInstance.setUserWithEmail(App::Persistence['user_email'], phoneNumber:User.phone.to_s, firstName:User.first_name, lastName:User.last_name, response:proc{|success, error| 
        NSLog "autho..."
        if success
          App::Persistence['rsosauth'] = true
          mp "[rapidSOS] - AUTH SUCCESS"
          check_for_contacts(forced)
        else
          mp "[rapidSOS] - AUTH ERROR"
          mp error  
        end                              
      })
    end  
  end 
  
  def config_contacts?
    App::Persistence["#{u_id}-contacts"] == true
  end     
  
  def u_id
    App::Persistence['user_id']
  end    
  
  def check_for_contacts(forced)
    get_contacts if !config_contacts? or forced
  end 
  
  def get_contacts
    mp "[rapidSOS] - GETTING CONTACTS"
    $contacts = []
    @data = {user_email: App::Persistence['user_email'], user_token: App::Persistence['authToken']}
    BW::HTTP.get("#{API_URL}/api/v1/users/#{App::Persistence['user_id']}/emergency_contacts", { payload: @data }) do |response|
      if response.ok?
        json = BW::JSON.parse(response.body.to_str)
        mp "[rapidSOS] - count #{json.count}"
        if json.count >= 1
          json.each do |contact|

            $contacts << {api_id: contact['id'], name: contact['name'], phone: contact['phone'], email: contact['email'], last_name: contact['really_last_name'].to_s}
            
          end
          set_rapidsos_contacts
        end
      else
        mp "[rapidSOS] - Error Getting Contacts"
        mp response
      end
    end
  end
  
  def set_rapidsos_contacts
    mp "[rapidSOS] - SETTING CONTACTS"
    EMGOneTouchManager.sharedInstance.deleteAllContactsWithCompletion(proc {|success, error|
      mp "[rapidSOS] - DELETING ALL CONTACTS"
      if success
        $contacts.each do |ec|
           contact = EMGUserContactDataModel.alloc.init
           contact.name = "#{ec[:name]} #{ec[:last_name]}"
           contact.phoneNumber = ec[:phone]
           contact.emailAddress = ec[:email]
           contact.priority = 0
           mp "[rapidSOS] - Creating contact with email: #{ec[:name]}"
           EMGOneTouchManager.sharedInstance.setContact(contact, completion:proc {|success, responseObject, error|   
              mp success
              if error
                mp "[rapidSOS] - Error Creating Contact"
                mp error
              end  
         
             mp "[rapidSOS] - Contact Created" if success           
           })
        end  
      end  
    })    
    App::Persistence["#{u_id}-contacts"] = true
  end    
  
end  
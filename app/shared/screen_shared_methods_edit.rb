module ScreenSharedMethodsEdit

  def registerNotifications
    UIKeyboardWillShowNotification.add_observer(self, 'keyboardWillToggleShow:')
    UIKeyboardWillHideNotification.add_observer(self, 'keyboardWillToggleHide:')
  end

  def textViewDidEndEditing(textView)
    textView.resignFirstResponder
  end

  def textViewShouldReturn(textView)
    textView.resignFirstResponder
    return false
  end

  def textViewDidBeginEditing(textView)
    @field = textView
  end

  def textFieldDidEndEditing(textField)
    textField.resignFirstResponder
  end
  def textFieldShouldReturn(textField)
    textField.resignFirstResponder
    return false
  end
  def textFieldDidBeginEditing(textField)
    @field = textField
  end

  def keyboardWillToggleShow(notification)
    if @field
      userInfo = notification.userInfo
      @keyboard_height = userInfo[UIKeyboardFrameBeginUserInfoKey].CGRectValue.size.height
      top = @field.frame.origin.y.to_i
      animate_screen_for_field(top)
      @field = nil
    end  
  end

  def keyboardWillToggleHide(notification)
    if @field    
      userInfo = notification.userInfo
      @keyboard_height = userInfo[UIKeyboardFrameBeginUserInfoKey].CGRectValue.size.height
      set_view_normal(self.view)
    end  
  end

  def animate_screen_for_field(top)
    move = (@keyboard_height) * -1
    if move < 0
      rmq(self.view).animate(
        duration: 0.3,
        animations: lambda{|v|
          v.move top: move + 200
        }
      )
    end
  end

  def set_view_normal(view)
    rmq(view).animate(
      duration: 0.3,
      animations: lambda{|v|
        v.style {|st| st.frame = {t:0}}
      }
    )
  end
end

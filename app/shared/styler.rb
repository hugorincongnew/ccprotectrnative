module RubyMotionQuery
  module Stylers
    
    class UIButtonStyler < UIControlStyler
      def underline=(enabled)
        if enabled
          titleString = NSMutableAttributedString.alloc.initWithString(@view.currentTitle)
          titleString.addAttribute(NSUnderlineStyleAttributeName, value:NSNumber.numberWithInteger(NSUnderlineStyleSingle), range:NSMakeRange(0, titleString.length))
          titleString.addAttribute(NSForegroundColorAttributeName, value:@view.titleColorForState(UIControlStateNormal), range:NSMakeRange(0, titleString.length))
          @view.setAttributedTitle(titleString, forState:UIControlStateNormal)
        end
      end
    end
    
    class UITextFieldStyler < UIControlStyler
      def left_padding=(number)
        if number
          paddingView = UIView.alloc.initWithFrame(CGRectMake(0, 0, number, 20))
          @view.leftView = paddingView
          @view.leftViewMode = UITextFieldViewModeAlways
        end  
      end  
    end  
  end
end

class ApplicationStylesheet < RubyMotionQuery::Stylesheet

  def application_setup

    # Change the default grid if desired
    #   rmq.app.grid.tap do |g|
    #     g.num_columns =  12
    #     g.column_gutter = 10
    #     g.num_rows = 18
    #     g.row_gutter = 10
    #     g.content_left_margin = 10
    #     g.content_top_margin = 74
    #     g.content_right_margin = 10
    #     g.content_bottom_margin = 10
    #   end

    # An example of setting standard fonts and colors
    font_family = 'Helvetica Neue'
    font.add_named :large,    font_family, 36
    font.add_named :medium,   font_family, 24
    font.add_named :small,    font_family, 18

    color.add_named :tint, '7ED321'
    color.add_named :translucent_black, color.from_rgba(0, 0, 0, 0.4)
    color.add_named :battleship_gray,   '#7F7F7F'
    color.add_named :blue_ish, '#3183A3'

  end

  def setting_icon_grey(st)
    st.frame = {left: w_p(87), top: h_p(6.7), width: w_p(8), height: w_p(8)}
    st.background_image_normal = UIImage.imageNamed("setting_icon_grey")
  end  

  def back_arrow(st)
    st.frame = {left: w_p(9), top: h_p(6.7), width: w_p(5.333), height: w_p(8.53)}
    st.background_image_normal = UIImage.imageNamed("back_arrow")
  end

  def standard_button(st)
    st.frame = {w: 80, h: 180}
    st.background_color = color.tint
    st.color = color.white
    st.font = font.system(22)
  end

  def end_button(st)
    st.frame = {w: 80, h: 180}
    st.background_color = color.blue_ish
    st.color = color.white
    st.font = font.system(22)
  end

  def panic_button(st)
    st.frame = {w: 80, h: 180}
    st.background_color = color.red
    st.color = color.white
    st.font = font.system(22)
  end

  def standard_label(st)
    st.frame = {w: 40, h: 18}
    st.background_color = color.clear
    st.color = color.black
  end
  
  def w_p(val)
    (Device.screen.width * val) / 100
  end
  
  def h_p(val)
    (Device.screen.height * val) / 100
  end   

end

class EditProfileScreenStylesheet < ApplicationStylesheet

  def root_view(st)
    st.background_color = rmq.color("#B5D5DF")

  end

  def edit_profile_title(st)
    st.frame = {t: h_p(6.7), width: rmq.device.width, height: 40 , centered: :horizontal}
    st.text = "Edit Profile"
    st.font = font.system(22)
    st.color = rmq.color("#888888")
    st.text_alignment = :center
  end    
  
  def first_name_field(st)
    st.frame = { grid: "a1", width: (rmq.device.width * 0.7), height: 40 , centered: :horizontal}
    st.color = UIColor.lightGrayColor
    st.corner_radius = 5
    st.font = font.system(14)
    st.placeholder = "First Name"
    st.background_color = rmq.color("#FFFFFF")
    st.enables_return_key_automatically = true
    st.allows_editing_text_attributes = true
    st.keyboard_type = UIKeyboardTypeDefault
    st.keyboard_appearance = UIKeyboardAppearanceDefault
    st.left_padding = 10
  end  
  
  def last_name_field(st)
    st.frame = { grid: "a3", width: (rmq.device.width * 0.7), height: 40 , centered: :horizontal}
    st.color = UIColor.lightGrayColor
    st.corner_radius = 5
    st.font = font.system(14)
    st.placeholder = "Last Name"
    st.background_color = rmq.color("#FFFFFF")
    st.enables_return_key_automatically = true
    st.allows_editing_text_attributes = true
    st.keyboard_type = UIKeyboardTypeDefault
    st.keyboard_appearance = UIKeyboardAppearanceDefault
    st.left_padding = 10 
  end  
  
  def phonenumber_field(st)
    st.frame = { grid: "a5", width: (rmq.device.width * 0.7), height: 40 , centered: :horizontal}
    st.color = UIColor.lightGrayColor
    st.corner_radius = 5
    st.font = font.system(14)
    st.placeholder = "Phone Number"
    st.background_color = rmq.color("#FFFFFF")
    st.enables_return_key_automatically = true
    st.allows_editing_text_attributes = true
    st.keyboard_type = UIKeyboardTypeDefault
    st.keyboard_appearance = UIKeyboardAppearanceDefault
    st.left_padding = 10  
  end      

  def promo_field(st)
    st.frame = { grid: "a7", width: (rmq.device.width * 0.7), height: 40 , centered: :horizontal}
    st.color = UIColor.lightGrayColor
    st.corner_radius = 5
    st.font = font.system(14)
    st.placeholder = "Promo Code"
    st.background_color = rmq.color("#FFFFFF")
    st.enables_return_key_automatically = true
    st.allows_editing_text_attributes = true
    st.keyboard_type = UIKeyboardTypeDefault
    st.keyboard_appearance = UIKeyboardAppearanceDefault
    st.left_padding = 10
  end
  
  def update_button(st)  
    st.frame = {grid: "a9", width: (rmq.device.width * 0.7), height: 40  , centered: :horizontal} 
    st.background_color = rmq.color("#12AF5D")
    st.color = color.white
    st.corner_radius = 10
    st.text = "Save"
    st.font = font.system(24)
  end
end

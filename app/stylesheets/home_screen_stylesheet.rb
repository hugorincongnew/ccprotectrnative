class HomeScreenStylesheet < ApplicationStylesheet

  def setup
    # Add sytlesheet specific setup stuff here.
    # Add application specific setup stuff in application_stylesheet.rb
  end

  def root_view(st)
    st.background_color = rmq.color("#B5D5DF")
    #st.background_image = image.resource('texture_1391578.jpg')

  end

  def hello_world(st)
    st.frame = {top: 100, width: 200, height: 18, centered: :horizontal}
    st.text_alignment = :center
    st.color = color.battleship_gray
    st.font = font.medium
    st.text = 'Radius'
  end

  def start_showing_button(st)
    # st.frame = {centered: :both}
    # st.text = 'Begin Showing'
    #st.color = rmq.color("#12AF5D")
    #st.image_normal = image.resource('begin_showing_button.png')
    st.background_color = rmq.color("#12AF5D")
    st.color = color.white
    st.frame = {grid:"c6:j7", centered: :horizontal} #{centered: :both, h: 80, w: 580}#{fr: 15, w: (st.superview.size.width - 45)/2, h: 40, t: 80}
    st.corner_radius = 10
    st.text = "Begin Showing"
    st.font = font.system(24)
  end

  def logo_pic(st)
  	st.image = image.resource('safetyIQicon@2x.png')
  	st.frame = {grid: "e0:i4", centered: :horizontal}
  end

  def logo(st)
    st.image = image.resource('iq_logo_shadow.png')
    st.frame = {grid: "f4:j6", height: 60, centered: :horizontal}
  end

  def grey_panic(st)
  	st.image = image.resource('grey_panic.png')
  	st.frame = {grid: "c8:j14", centered: :horizontal}
  end

  def em_contacts_bar(st)
  	st.image = image.resource('em_contacts_bar.png')
  	st.frame = {grid: "a16:l17", w: :full, centered: :horizontal}
  end

  def logout_button(st)
  	st.image = image.resource('logout_button.png')
  	st.frame = {grid: "e15:h15", centered: :horizontal, padding: {l: 5, t: 0, r: 5, b: 0}}
  end

  def background_pic(st)

  end

end

class LoginScreenStylesheet < ApplicationStylesheet
  # Add your view stylesheets here. You can then override styles if needed,
  # example: include FooStylesheet

  def setup
    # Add sytlesheet specific setup stuff here.
    # Add application specific setup stuff in application_stylesheet.rb
  end

  def root_view(st)
    st.background_color = rmq.color("#d4e8f8")
  end

  def login(st)
    #st.background_color = rmq.color("#12AF5D")
    st.color = rmq.color("#00ab4f")
    st.frame = {grid:"i13", width: 100, height: 40}
    st.corner_radius = 10
    st.text = "Sign in >"
    st.font = font.system(20)
  end

  def lost_password(st)
    st.color = rmq.color("#007fc4")
    st.frame = {grid:"b13", width: 150, height: 40}
    st.text = "Lost Your Password?"
    st.font = font.system(14)
    st.underline = true
  end

  def logo_pic(st)
  	st.image = image.resource('bg_logo.png')
  	st.frame = {grid: "b0:j7", centered: :horizontal}
  end

  def logo(st)
  	st.image = image.resource('iq_logo_shadow.png')
  	st.frame = {grid: "b6:l10", centered: :horizontal}
  end

  def text_login(v)
    v.frame = { grid: "a11", width: 300, height: 26 , centered: :horizontal}
    v.color = UIColor.lightGrayColor
    #v.autocorrectionType = UITextAutocorrectionTypeNo
    v.corner_radius = 12
    v.font = font.system(14)
    v.placeholder = "Email"
    v.background_color = rmq.color("#FFFFFF")
    v.enables_return_key_automatically = true
    v.allows_editing_text_attributes = true
    v.keyboard_type = UIKeyboardTypeDefault
    v.keyboard_appearance = UIKeyboardAppearanceDefault
    v.left_padding = 10
  end

  def text_password(v)
    v.frame  = { grid: "a12", width: 300, height: 26, centered: :horizontal}
    v.color = UIColor.lightGrayColor
    #v.autocorrectionType = UITextAutocorrectionTypeNo
    v.corner_radius = 12
    v.font = font.system(14)
    v.secure_text_entry = true
    v.placeholder = "Password"
    v.background_color = rmq.color("#FFFFFF")
    v.keyboard_type = UIKeyboardTypeDefault
    v.keyboard_appearance = UIKeyboardAppearanceDefault
    v.left_padding = 10
  end

  def label_style(st)
    st.frame = {grid: "a15:j15", w: :full, h: 60, centered: :horizontal}
    st.background_color = rmq.color("#007fc4")
    #st.text = "If you dont'n have an account: Sign Up"
  end

  def text_in_label(st)
    st.frame = {grid: "a15:j15", h: 60, centered: :horizontal}
    st.text = "If you don't have an account:"
    st.font = font.system(16)
    st.color = color.white
  end

  def sign_up(st)
    st.frame = {grid: "h15:j18", h: 60, w: 120, padding: {l: 64, t: 0, r: 0, b: 0}}
    st.text = "Sign Up"
    st.font = font.system(15)
    st.color = color.white
    st.underline = true
  end
end

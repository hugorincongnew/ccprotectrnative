class ManageSubscriptionsScreenStylesheet < ApplicationStylesheet

  def root_view(st)
    st.background_color = rmq.color("#B5D5DF")

  end

  def subcriptions_title(st)
    st.frame = {t: h_p(6.7), width: rmq.device.width, height: 40 , centered: :horizontal}
    st.text = "Your Subscription"
    st.font = font.system(22)
    st.color = rmq.color("#888888")
    st.text_alignment = :center
  end    

end

class RegisterScreenStylesheet < ApplicationStylesheet

	def root_view(st)
    st.background_color = rmq.color("#d4e8f8")
  end

  def login(st)
    st.frame = {grid:"i13", width: 100, height: 40}
    st.text = "Register"
    st.color = rmq.color("#00ab4f")
    st.corner_radius = 10
    st.font = font.system(20)
  end

  def logo_pic(st)
  	st.image = image.resource('bg_logo.png')
  	st.frame = {grid: "b0:j7", centered: :horizontal}
  end

  def logo(st)
  	st.image = image.resource('iq_logo_shadow.png')
  	st.frame = {grid: "b6:l10", centered: :horizontal}
  end

  def text_login(v)
    v.frame = { grid: "a11", width: 300, height: 26 , centered: :horizontal}
    v.color = UIColor.lightGrayColor
    #v.autocorrectionType = UITextAutocorrectionTypeNo
    v.corner_radius = 12
    v.font = font.system(14)
    v.placeholder = "Email"
    v.background_color = rmq.color("#FFFFFF")
    v.left_padding = 10
  end

  def text_password(v)
    v.frame  = { grid: "a12", width: 300, height: 26, centered: :horizontal}
    v.color = UIColor.lightGrayColor
    #v.autocorrectionType = UITextAutocorrectionTypeNo
    v.corner_radius = 12
    v.font = font.system(14)
    v.placeholder = "Password"
    v.secure_text_entry = true
    v.background_color = rmq.color("#FFFFFF")
    v.left_padding = 10
  end

  def label_style(st)
    st.frame = {grid: "a15:j15", w: :full, h: 60, centered: :horizontal}
    st.background_color = rmq.color("#007fc4")
    #st.text = "If you dont'n have an account: Sign Up"
  end

  def text_in_label(st)
    st.frame = {grid: "a15:j15", h: 60, centered: :horizontal}
    st.text = "If you do have an account:"
    st.font = font.system(16)
    st.color = color.white
  end

  def log_in(st)
    st.frame = {grid: "g15:j15", h: 60, w: 120, padding: {l: 64, t: 0, r: 0, b: 0}}
    st.text = "Log in"
    st.font = font.system(16)
    st.color = color.white
    st.underline = true
  end
end

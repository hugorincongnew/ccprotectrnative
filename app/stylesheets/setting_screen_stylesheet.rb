class SettingScreenStylesheet < ApplicationStylesheet

  def setup
    # Add sytlesheet specific setup stuff here.
    # Add application specific setup stuff in application_stylesheet.rb
  end

  def root_view(st)
    st.background_color = rmq.color("#B5D5DF")

  end
  
  def edit_profile_button(st)
    st.background_color = rmq.color("#12AF5D")
    st.color = color.white
    st.frame = {grid:"b2:k3", centered: :horizontal} #{centered: :both, h: 80, w: 580}#{fr: 15, w: (st.superview.size.width - 45)/2, h: 40, t: 80}
    st.corner_radius = 10
    st.text = "Edit Account"
    st.font = font.system(24)
  end  
  
  def manage_subscriptions_button(st)
    st.background_color = rmq.color("#12AF5D")
    st.color = color.white
    st.frame = {grid:"b4:k5", centered: :horizontal} #{centered: :both, h: 80, w: 580}#{fr: 15, w: (st.superview.size.width - 45)/2, h: 40, t: 80}
    st.corner_radius = 10
    st.text = "Manage Subscriptions"
    st.font = font.system(24)
  end  

  def emergency_contacts_button(st)
    st.background_color = rmq.color("#12AF5D")
    st.color = color.white
    st.frame = {grid:"b6:k7", centered: :horizontal} #{centered: :both, h: 80, w: 580}#{fr: 15, w: (st.superview.size.width - 45)/2, h: 40, t: 80}
    st.corner_radius = 10
    st.text = "Emergency Contacts"
    st.font = font.system(24)
  end


  def logout_button(st)
    st.background_color = rmq.color("#12AF5D")
    st.color = color.white
    st.frame = {grid:"b9:k10", centered: :horizontal} #{centered: :both, h: 80, w: 580}#{fr: 15, w: (st.superview.size.width - 45)/2, h: 40, t: 80}
    st.corner_radius = 10
    st.text = "Logout"
    st.font = font.system(24)
  end

end

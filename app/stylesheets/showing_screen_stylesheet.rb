class ShowingScreenStylesheet < ApplicationStylesheet
  # Add your view stylesheets here. You can then override styles if needed,
  # example: include FooStylesheet

  def setup
    # Add sytlesheet specific setup stuff here.
    # Add application specific setup stuff in application_stylesheet.rb
  end

  def end_showing_button(st)
    # st.frame = {centered: :both}
    # st.text = 'Begin Showing'
    #st.color = rmq.color("#12AF5D")
    #st.image_normal = image.resource('begin_showing_button.png')
    st.background_color = rmq.color("#12AF5D")
    st.color = color.white
    st.frame         = {grid:"c5:j6", centered: :horizontal} #{centered: :both, h: 80, w: 580}#{fr: 15, w: (st.superview.size.width - 45)/2, h: 40, t: 80}
    st.corner_radius = 10
    st.text = "End Showing"
  end

  def logo_pic(st)
  	st.image = image.resource('safetyIQicon@2x.png')
  	st.frame = {grid: "e0:i3", centered: :horizontal}
  end

  def panic_button_style(st)
  	st.image = image.resource('red_panic.png')
  	st.frame = {grid: "c8:j14", centered: :horizontal}
  end

  def wave_one(st)
  	st.image = image.resource('wave-one')
  	st.frame = {grid: "a1:i4", centered: :horizontal}
  end

  def wave_two(st)
  	st.image = image.resource('wave-two')
  	st.frame = {grid: "a1:i4", centered: :horizontal}
  end

  def wave_three(st)
  	st.image = image.resource('wave-full')
  	st.frame = {grid: "a1:i4", centered: :horizontal}
  end

  def root_view(st)
    st.background_color = rmq.color("#B5D5DF")
    #st.background_image = image.resource('texture_1391578.jpg')
  end
end

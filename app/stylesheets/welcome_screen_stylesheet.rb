class WelcomeScreenStylesheet < ApplicationStylesheet
  # Add your view stylesheets here. You can then override styles if needed,
  # example: include FooStylesheet

  def setup
    # Add stylesheet specific setup stuff here.
    # Add application specific setup stuff in application_stylesheet.rb
  end

  def logo_pic(st)
  	st.image = image.resource('bg_logo.png')
  	st.frame = {grid: "b1:j8", centered: :horizontal}
  end

  def logo(st)
  	st.image = image.resource('iq_logo_shadow.png')
  	st.frame = {grid: "b7:l11", centered: :horizontal}
  end

  def login(st)
    st.background_color = rmq.color("#12AF5D")
    st.color = color.white
    st.frame         = {grid:"b13:k14", centered: :horizontal} #{centered: :both, h: 80, w: 580}#{fr: 15, w: (st.superview.size.width - 45)/2, h: 40, t: 80}
    st.corner_radius = 10
    st.text = "Login"
  end

  def register(st)
    st.background_color = rmq.color("#12AF5D")
    st.color = color.white
    st.frame         = {grid:"b15:k16", centered: :horizontal} #{centered: :both, h: 80, w: 580}#{fr: 15, w: (st.superview.size.width - 45)/2, h: 40, t: 80}
    st.corner_radius = 10
    st.text = "Register"
  end

  def root_view(st)
    st.background_color = rmq.color("#d4e8f8")
  end
end

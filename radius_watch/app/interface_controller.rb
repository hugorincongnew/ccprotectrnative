class Listing
    @priceFormatter ||= begin
      formatter = NSNumberFormatter.new
      formatter.currencyCode = 'USD'
      formatter.numberStyle = NSNumberFormatterCurrencyStyle
      formatter
    end
end

class InterfaceController < WKInterfaceController
  extend IB

  outlet :addressLabel, WKInterfaceLabel
  outlet :priceLabel, WKInterfaceLabel
  outlet :okButton, WKInterfaceButton

  def initWithContext(context)
    super
    @listing_address = "123 Main St"
    @listing_price = 500000
    #updatePrice(@listing_price)
    # Initialize variables here.
    # Configure interface objects here.
    #updatePrice
    @priceLabel.text = "$500,000"
    @addressLabel.text = "123 Fake St."
    #p Listing.price
    sleep 1
    NSLog("%@ initWithContext", self)

    self
  end

  def willActivate
    # This method is called when watch view controller is about to be visible to user
    NSLog("%@ will activate", self)
  end

  def didDeactivate
    # This method is called when watch view controller is no longer visible
    NSLog("%@ did deactivate", self)
  end

  def updatePrice
    @priceLabel.text = 500000
  end

  def buttonTapped
    p "Tapped the watch button"
    #Radius::app.app_delegate.begin_showing
  end

  # def updatePrice(price)
  #   @priceLabel.text = @listing_price
  # end

end

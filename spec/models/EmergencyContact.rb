describe 'Emergencycontact' do

  before do
    class << self
      include CDQ
    end
    cdq.setup
  end

  after do
    cdq.reset!
  end

  it 'should be a Emergencycontact entity' do
    Emergencycontact.entity_description.name.should == 'Emergencycontact'
  end
end

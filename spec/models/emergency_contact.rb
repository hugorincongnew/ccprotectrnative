describe 'EmergencyContact' do

  before do
    class << self
      include CDQ
    end
    cdq.setup
  end

  after do
    cdq.reset!
  end

  it 'should be a EmergencyContact entity' do
    EmergencyContact.entity_description.name.should == 'EmergencyContact'
  end
end
